# openshift-spring-boot


Sample Spring Boot App deployable to OpenShift nextgen (v3)

Deploys maven built war utilizing [OpenShifts Source-to-Image (S2I)](https://docs.openshift.com/enterprise/3.0/architecture/core_concepts/builds_and_image_streams.html#source-build) 

###### Create new OpenShift Wildfly app

	oc new-app wildfly:10.1~https://zuchka@bitbucket.org/zuchka/namegen.git
	
	oc new-app openjdk18-web-basic-s2i:1.0~https://zuchka@bitbucket.org/zuchka/namegen.git